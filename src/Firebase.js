import firebase from 'firebase';

const config = {
    // Your web app's Firebase configuration
    apiKey: "AIzaSyByspHlWNPXsyiJn1qCJjraAz4pugQC-GU",
    authDomain: "lazy-load-95826.firebaseapp.com",
    databaseURL: "https://lazy-load-95826.firebaseio.com",
    projectId: "lazy-load-95826",
    storageBucket: "lazy-load-95826.appspot.com",
    messagingSenderId: "349447639466",
    appId: "1:349447639466:web:cea8652dad028a65f85bd3",
    measurementId: "G-QW80XZFG7S"
};

const firebaseConf = firebase.initializeApp(config);
export default firebaseConf;