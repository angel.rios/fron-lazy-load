import React from 'react'
import firebaseConf from '../../Firebase'
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom'

import { Form, Input, FormGroup, Label, Grid, Header, Image } from 'semantic-ui-react'
const uuid = require('uuid');

class Inputs extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: '', cedula: '',
      uid: uuid.v1(),
      lazyDateRegistration: '',
      ref: false
    };
    //this.handleSubmit = this.handleSubmit.bind(this);
    this.submitData = this.submitData.bind(this);
    this.inputData = this.inputData.bind(this);

  }
  submitData(event) {
    event.preventDefault();


    firebaseConf
      .database()
      .ref(`users/${this.state.uid}`)
      .set({
        identification: this.state.cedula,
        lazyDateRegistration: this.state.lazyDateRegistration,
      }
      ).then(()=>{
        this.setState({ref:true})
      })
      

      
      
      
    }
    componentDidUpdate(){
      if(this.state.ref==true){
        console.log("redre");
        window.location.href="/inputFile";
      }
    }
 
  
  
  inputData(event) {
    const lazyDate = new Date();
    const lazyDateRegistration = lazyDate.toString();
    this.setState({ cedula: event.target.value, lazyDateRegistration });
  }

  validateLenghtCedula() {
    const { cedula } = this.state;
    return cedula.length >= 9 && cedula.length <= 11;
  }
  onInput = (event) => {
    event.target.value = Math.max(0, parseInt(event.target.value)).toString().slice(0, 11)
  }
  sendPage() {
    {   window.location.href = 'inputFile'
    }
  }



  render() {
    const isEnabled = this.validateLenghtCedula();
    return (
      <div className="col-sm-15 offset-sm-3  mb-5 text-center">
        <React.Fragment>

          <Grid columns={2} only="tablet computer">

            <Grid.Column width={1}>
              <Label as='a' color='teal'>
                #
                <Label.Detail>Cédula</Label.Detail>
              </Label>
            </Grid.Column >
            <Grid.Column width={8}  >
              <Form onSubmit={this.submitData} >
                <FormGroup>
                  <Form.Field
                    value={this.state.cedula}
                    id="input-cedula"
                    control={Input}
                    onInput={this.onInput}
                    type="number"
                    maxLength={10}
                    width={6}
                    onChange={this.inputData}
                    placeholder="número de cédula"
                    error={!isEnabled && {
                      content: "La cédula debe tener mínimo 9 y máximo 11  dígitos",
                      pointing: "left"
                    }}
                  />
                  <Grid.Column>
                   
                    {isEnabled ? <a class="btn btn-success" 
                    disabled={!isEnabled} onClick ={this.submitData} role="button" >Ingresar</a> :
                     <a class="btn btn-secondary"onClick 
                     role="button">Ingresar</a>}

                   
                  </Grid.Column>
                </FormGroup>
              </Form>
            </Grid.Column>
          </Grid>
        </React.Fragment >
      </div>

    );
  }
}



export default Inputs;

