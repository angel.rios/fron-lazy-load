import React, { Component } from 'react'
import axios from 'axios'

export default class InputFile extends Component {

    state = {
        selectedFile: null, loaded: 0, fileResponse: "", isLoggedIn: false
    }

    handlSelectedFile = event => {
        this.setState({
            selectedFile: event.target.files[0],
            loaded: 0,
            fileResponse: "",
        })
    }


    handleUpload = () => {
        const data = new FormData()
        data.append('file', this.state.selectedFile, this.state.selectedFile.name)
        axios.post("http://localhost:9090/lazyLoad/file/elementsByDay", data, {
            onUploadProgress: ProgressEvent => { this.setState({ loaded: (ProgressEvent.loaded / ProgressEvent.total * 100), }) },
        }).then(res => {
            this.setState({ fileResponse: res.data })
            this.downloadTxtFile()
        }).then(()=>window.location.href="/")
    }



    downloadTxtFile = () => {
        const element = document.createElement("a");
        const file = new Blob([this.state.fileResponse]);
        element.href = URL.createObjectURL(file);
        element.download = "lazyLoadResult.txt";
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }


    render() {
        return (
            <div className="App">
                <div className="col-sm-5 offset-sm-3  mb-1 text-center" >
                    <div> {Math.round(this.state.loaded, 2)}% </div>
                </div>

                <div className="col-sm-5 offset-sm-3  mb-3 text-center" >
                    <input type="file" className="form-control" aria-label="Username" aria-describedby="basic-addon1" onChange={this.handlSelectedFile} />
                </div>

                <div className="col-sm-5 offset-sm-3  mb-10 text-center">
                    {this.state.selectedFile != null ? (<a class="btn btn-primary" role="button" 
                    onClick={() => { this.handleUpload(); }}>Procesar</a>) : ("")}
               
                </div>

            </div>


        )
    }
}