import React, { Component } from 'react'

export default class InputFile extends Component {
    render() {
        return (
            <div className="bd-example">
                <div className="input-group mb-3">
                    <div className="custom-file">
                        <input type="file" className="custom-file-input" id="inputFile" />
                        <label className="custom-file-label" Htmlfor="inputFile">Upload</label>
                    </div>
                </div>

            </div>
        )
    }
}
