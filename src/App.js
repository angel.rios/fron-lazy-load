import React from 'react';
import './App.css';
import InputFile from "./components/InputFile"
import 'bootstrap/dist/css/bootstrap.min.css';
import InputPersonalId from './components/inputPersonalId/InputPersonalId';
import { Header } from 'semantic-ui-react';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom"

function App() {
  return (
    <div>
      <Header />
      <Router>
        <Route exact path="/inputFile">
          <InputFile />
        </Route>
        <Route exact path="/">
          <InputPersonalId />
          
        </Route>
      </Router>


    </div>
  );
}

export default App;
